SHELL :=/bin/bash -O extglob
EPUB = le-baton-de-pluie.epub
MOBI = $(patsubst %.epub,%.mobi,$(EPUB))

all: $(EPUB) mobi check

epub $(EPUB): clean
	$(shell printf "cd src; zip -Xr9D $(EPUB) mimetype *; mv $(EPUB) ..")

.PHONY: mobi
mobi $(MOBI): $(EPUB)
	@printf "\nConverting from ePub to MOBI now...\n"
	@$(shell echo /Applications/calibre.app/Contents/MacOS/ebook-convert $(EPUB) $(MOBI))

.PHONY: clean
clean:
	$(RM) $(EPUB)
	$(RM) $(MOBI)
	@$(shell find . -name ".DS_Store" -depth -exec rm {} \;)

.PHONY: test check
test check: $(EPUB)
	@printf "\nTesting %s now...\n" $(EPUB)
	@$(shell printf "epubcheck $(EPUB)")

print-%: ; @echo $*=$($*)
